<?php

namespace designerei\ContaoBannerBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\FrontendTemplate;
use Contao\Input;

/**
 * @Hook("parseFrontendTemplate")
 */
class OutputBannerListener
{
    public function __invoke(string $buffer, string $templateName, FrontendTemplate $template): string
    {
        if (str_starts_with($templateName, 'mod_banner'))
        {
            // check if cookie is set
            $cookieValue = $template->id;
            $cookieName = "banner_closed_" . $cookieValue;
            $cookieExpires = $template->expires;

            // get cookie
            $cookie = Input::cookie($cookieName);

            // check if banner is enabled
            if ($template->enableBanner)
            {

                // check if cookie is set (banner is closed)
                if ($cookie && $cookieExpires > 0)
                {
                    $buffer = '';
                }

                else
                {

                    // check selected pages
                    if ($template->selectPages) {

                        // get current page id
                        $currentPage = $GLOBALS['objPage']->id;
                        $currentPage = intval($currentPage);

                        $selectedPages = deserialize($template->pages);

                        if (!in_array($currentPage, $selectedPages)) {
                            $buffer = '';
                        }
                    }
                }

            }

            else
            {
                $buffer = '';
            }
        }

        return $buffer;
    }
}