<?php

$table = 'tl_module';

$GLOBALS['TL_DCA'][$table]['palettes']['__selector__'][] = 'addImage';
$GLOBALS['TL_DCA'][$table]['palettes']['__selector__'][] = 'addLink';
$GLOBALS['TL_DCA'][$table]['palettes']['__selector__'][] = 'enableBanner';
$GLOBALS['TL_DCA'][$table]['palettes']['__selector__'][] = 'selectPages';

$GLOBALS['TL_DCA'][$table]['palettes']['banner'] =
    '{title_legend},name,type;'
    . '{content_legend},bannerText,addImage,addLink;'
    . '{banner_legend},enableBanner;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests,cssID;'
;

$GLOBALS['TL_DCA'][$table]['subpalettes']['addImage'] = 'singleSRC,imgSize';
$GLOBALS['TL_DCA'][$table]['subpalettes']['addLink'] = 'linkUrl, linkTarget, linkTitle, titleText';
$GLOBALS['TL_DCA'][$table]['subpalettes']['enableBanner'] = 'selectPages, cookieExpires';
$GLOBALS['TL_DCA'][$table]['subpalettes']['selectPages'] = 'pages';

$GLOBALS['TL_DCA'][$table]['fields']['bannerText'] = [
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'textarea',
    'eval'                    => array('mandatory'=>true, 'allowHtml'=>true),
    'sql'                     => "mediumtext NULL"
];

$GLOBALS['TL_DCA'][$table]['fields']['addImage'] = [
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('submitOnChange'=>true),
    'sql'                     => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA'][$table]['fields']['addLink'] = [
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('submitOnChange'=>true),
    'sql'                     => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA'][$table]['fields']['linkUrl'] = [
    'label'                   => &$GLOBALS['TL_LANG']['MSC']['url'],
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>true, 'rgxp'=>'url', 'decodeEntities'=>true, 'maxlength'=>2048, 'dcaPicker'=>true, 'tl_class'=>'w50'),
    'sql'                     => "varchar(2048) NOT NULL default ''"
];

$GLOBALS['TL_DCA'][$table]['fields']['linkTarget'] = [
    'label'                   => &$GLOBALS['TL_LANG']['MSC']['target'],
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'w50 m12'),
    'sql'                     => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA'][$table]['fields']['linkTitle'] = [
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['linkTitle'],
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA'][$table]['fields']['titleText'] = [
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['titleText'],
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA'][$table]['fields']['enableBanner'] = [
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('submitOnChange'=>true),
    'sql'                     => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA'][$table]['fields']['selectPages'] = [
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('submitOnChange'=>true, 'tl_class'=>'w50'),
    'sql'                     => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA'][$table]['fields']['cookieExpires'] = [
    'exclude'                 => true,
    'default'                 => 30,
    'inputType'               => 'text',
    'eval'                    => array('rgxp'=>'natural', 'mandatory'=>true, 'tl_class'=>'w50 clr'),
    'sql'                     => "smallint(5) unsigned NOT NULL default '0'"
];