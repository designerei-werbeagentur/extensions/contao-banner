function createCookie(name, value, days) {
    let expires;
    if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function closeBanner() {
    banner.style.display = "none";
}

// click on close button
const banner = document.getElementById("mod_banner");
const closeButton =  document.getElementById("closeBanner");

if(banner) {
    let intID = banner.dataset.moduleid;
    let expires = banner.dataset.expires;
    closeButton.addEventListener("click", function () {
        closeBanner();
        createCookie("banner_closed_" + intID, '1', expires);
    });
}

