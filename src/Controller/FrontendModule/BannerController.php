<?php

namespace designerei\ContaoBannerBundle\Controller\FrontendModule;

use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\ModuleModel;
use Contao\Template;
use Contao\FilesModel;
use Contao\Controller;
use Contao\StringUtil;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
* @FrontendModule("banner",
*   category="miscellaneous"
* )
*/
class BannerController extends AbstractFrontendModuleController
{
    protected function getResponse(Template $template, ModuleModel $model, Request $request): ?Response
    {
        // text
        $template->text = StringUtil::toHtml5($model->text);

        // image
        if ($model->addImage)
        {
            $image = FilesModel::findByUuid($model->singleSRC);

            $singleSRC = $image->path;
            $size = $model->imgSize;

            Controller::addImageToTemplate($template, [
                'singleSRC' => $singleSRC,
                'size' => $size,
            ], null, null, $image);

            $template->singleSRC = $image->path;
            $template->size = $model->imgSize;
        }


        // hyperlink
        if ($model->addLink)
        {
            if (0 === strncmp($model->linkUrl, 'mailto:', 7))
            {
                $model->linkUrl = StringUtil::encodeEmail($model->linkUrl);
            }

            else
            {
                $model->linkUrl = StringUtil::ampersand($model->linkUrl);
            }

            if (!$model->linkTitle)
            {
                $model->linkTitle = $model->linkUrl;
            }

            $template->href = $model->linkUrl;
            $template->link = $model->linkTitle;
            $template->target = '';
            $template->rel = '';

            if ($model->titleText)
            {
                $template->linkTitle = StringUtil::specialchars($model->titleText);
            }

            if ($model->linkTarget)
            {
                $template->target = ' target="_blank"';
                $template->rel = ' rel="noreferrer noopener"';
            }
        }

        if($model->enableBanner)
        {
            // cookies
            $template->expires = $model->cookieExpires;

            // add public assets javascript assets to fe_page
            $GLOBALS['TL_BODY'][] = \Contao\Template::generateScriptTag('bundles/contaobanner/banner.js', false, null);
        }

        return $template->getResponse();
    }
}
