# Contao-Banner

This extension for Contao Open Source CMS allows you to create a banner component as frontend module. By default, the element consists of a text, an image (optional), a link (optional) and a close button. By clicking the close button, a cookie will be set. You can enable or disable the banner in the backend settings, select explicit pages and set the expiry date for the cookie.  

## Function & usage

To use the banner you can embed the banner within in the `fe_page` template, within the layout settings or directly within the article as element type `module`.